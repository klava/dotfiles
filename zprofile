emulate sh -c '. ~/.profile'

if [ -z "$DISPLAY" ] && [ "$(tty)" = "/dev/tty1" ] && [ -z "$SSH_CONNECTION" ] && [ -n "$XDG_VTNR" ] && command -v sway > /dev/null && [ "$XDG_VTNR" -eq 1 ]; then
    unset DISPLAY
    export XDG_CURRENT_DESKTOP=sway
    exec dbus-run-session sway
fi
