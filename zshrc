# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh
ZSH_CUSTOM=$HOME/.oh-my-zsh-custom

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.

# ZSH_THEME="af-magic"
# ZSH_THEME="agnoster"
# ZSH_THEME="frisk"
# ZSH_THEME="minimal"
# ZSH_THEME="robbyrussell"
# ZSH_THEME="steeef"
# ZSH_THEME="steeef"
ZSH_THEME="jtriley"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
COMPLETION_WAITING_DOTS="true"

# vi-mode 10 ms delay to normal mode
KEYTIMEOUT=1

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git golang sudo ssh-agent node cp docker docker-compose
  kubectl python zsh-syntax-highlighting zsh-autosuggestions)

source $ZSH/oh-my-zsh.sh
unsetopt correct_all
unsetopt share_history

# Increase file limit
ulimit -n 1024

# Aliases
alias ga="git add -A"
alias gc="git commit --verbose"
alias gch="git checkout"
alias gd="git diff --patience --cached -w"
alias gl="git log --pretty=format:'%C(yellow)%h%Cred%d%Creset - %C(cyan)%an %Creset%G? : %s %Cgreen(%cr)'"
alias gls="gl --stat"
alias gp="git push"
alias gs="echo -n '\033[0;33m'; git branch --show-current 2>/dev/null || (echo 'No repo' && exit 1); echo -n '\033[0;0m'; git status -s 2>/dev/null"
alias gu="git pull -X patience --rebase"
alias now="date +%T"
alias k=kubectl
alias jfrog="docker run --user $(id -u):$(id -g) --rm -t -v $HOME:$HOME -e JFROG_CLI_LOG_LEVEL=ERROR -e JFROG_CLI_REPORT_USAGE=false -e JFROG_CLI_HOME_DIR=/jfrog -v $HOME/.config/jfrog:/jfrog docker.bintray.io/jfrog/jfrog-cli-go:latest jfrog"
alias dc="docker compose"
alias dc-run="docker compose run --rm"
alias dc-rm="docker compose rm"
alias dr="docker run --rm -it"
alias crl="curl --fail-with-body -sS"

function dc-restart {
  docker compose kill $@
  dc-rm --force $@
  dc up -d $@
  timeout --signal INT 5 docker compose logs --tail 10 --follow $@
}

function precmd {
  print -Pn "\e]133;A\e\\"

  if ! builtin zle; then
    print -n "\e]133;D\e\\"
  fi
}

function preexec {
  print -n "\e]133;C\e\\"
}

# Allows glob renameing - BEST THING EVER
autoload -U zmv
alias mmv='noglob zmv -W'

export TERM=vt100

# Preferred editor for local and remote sessions
if [ -x "$(command -v batcat)" ]; then
  alias cat=batcat
fi

# Preferred editor for local and remote sessions
if [ -x "$(command -v nvim)" ]; then
  export EDITOR='nvim'
  export GIT_EDITOR='nvim'
fi

# Postgresql location
if [ -x "$(command -v postgres)" ]; then
  export PGDATA=/usr/local/var/postgres
fi

# OCAML autocompletion
if [ -x "$(command -v opam)" ]; then
  source $HOME/.opam/opam-init/init.zsh >/dev/null 2>/dev/null || true

  eval $(opam env >/dev/null 2>/dev/null || true)
fi

# Node stuff
if [ -x "$(command -v yarn)" ]; then
  export PATH=$PATH:$(yarn global bin)
fi
if [ -x "$(command -v npm)" ]; then
  NPM_PACKAGES=$HOME/.npm-global

  if [ ! -d $NPM_PACKAGES ]; then
    mkdir -p $NPM_PACKAGES
    npm config set prefix "$NPM_PACKAGES"
    echo "Configured NPM to use prefix $NPM_PACKAGES"
  fi

  export MANPATH="${MANPATH-$(manpath)}:$NPM_PACKAGES/share/man"
fi

# Dotnet / Microsoft funky business
if [ -x "$(command -v dotnet)" ]; then
  export DOTNET_CLI_TELEMETRY_OPTOUT=1
fi

# Cargo installs
if [ -f $HOME/.cargo/env ]; then
  . "$HOME/.cargo/env" >&/dev/null
fi

# Atuin shell history https://github.com/atuinsh/atuin
if [ -x "$(command -v atuin)" ]; then
  eval "$(atuin init zsh)"
fi

# Ensure GPG can prompt for codes etc.
export GPG_TTY=$(tty)
