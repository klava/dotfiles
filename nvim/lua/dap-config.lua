local dap, dapui = require("dap"), require("dapui")

-- dap.set_log_level("TRACE")

require("nvim-dap-virtual-text").setup({ enabled_commands = false })

local M = {}
local filter = "" -- allows rerunning without filter
local is_rerun = false

-- continue or start a debugging session
-- will load .vscode/launch.json files if available
M.continue = function()
    -- needed in order to map type=delve to the go filetype
    require("dap.ext.vscode").load_launchjs(nil, { delve = { "go" } })
    dap.continue()
end

-- rerun the last dap invocation
M.rerun = function()
    is_rerun = true
    dap.run_last()
    is_rerun = false
end

dapui.setup({
    layouts = {
        {
            elements = {
                { id = "scopes",      size = 0.55 },
                { id = "breakpoints", size = 0.15 },
                { id = "stacks",      size = 0.15 },
                { id = "watches",     size = 0.15 },
            },
            position = "left",
            size = 60,
        },
        {
            elements = { { id = "repl", size = 0.5 }, { id = "console", size = 0.5 } },
            position = "bottom",
            size = 10,
        },
    },
})

dap.listeners.after.event_initialized["dapui_config"] = function() dapui.open() end
dap.listeners.before.event_terminated["dapui_config"] = function() dapui.close() end
dap.listeners.before.event_exited["dapui_config"] = function() dapui.close() end

dap.adapters.go = {
    type = "server",
    port = "${port}",
    executable = { command = "dlv", args = { "dap", "-l", "127.0.0.1:${port}" } },
}

dap.adapters.python = function(cb, config)
    local final = {
        type = "server",
        port = "${port}",
        executable = {
            detached = false,
            command = ".venv/bin/python",
            args = { "-m", "debugpy", "--listen", "${port}", "--wait-for-client" },
        }
    }

    for _, arg in ipairs(vim.deepcopy(config.args or {})) do
        table.insert(final.executable.args, arg)
    end

    -- print("final:", vim.inspect(final))

    cb(final)
end


-- - `${file}`: Active filename
-- - `${fileBasename}`: The current file's basename
-- - `${fileBasenameNoExtension}`: The current file's basename without extension
-- - `${fileDirname}`: The current file's dirname
-- - `${fileExtname}`: The current file's extension
-- - `${relativeFile}`: The current file relative to |getcwd()|
-- - `${relativeFileDirname}`: The current file's dirname relative to |getcwd()|
-- - `${workspaceFolder}`: The current working directory of Neovim
-- - `${workspaceFolderBasename}`: The name of the folder opened in Neovim
-- - `${command:pickProcess}`: Open dialog to pick process using |vim.ui.select|
-- - `${command:pickFile}`: Open dialog to pick file using |vim.ui.select|
-- - `${env:Name}`: Environment variable named `Name`, for example: `${env:HOME}`.

dap.configurations.go = {
    {
        type = "go",
        name = "Debug test (go.mod + filter)",
        request = "launch",
        mode = "test",
        program = "./${relativeFileDirname}",
        args = {
            "-test.count",
            "1",
            "-test.run",
            function()
                if is_rerun then return filter end
                vim.ui.input({ prompt = "-run " },
                    function(value) if value ~= nil then filter = value end end)
                return filter
            end,
        },
    },
    { type = "go", name = "Debug",      request = "launch", program = "${file}" },
    { type = "go", name = "Debug test", request = "launch", mode = "test",      program = "${file}" },
    {
        type = "go",
        name = "Debug test (go.mod)",
        request = "launch",
        mode = "test",
        program = "./${relativeFileDirname}",
    },
}

dap.configurations.python = {
    {
        type = "python",
        name = "Run",
        request = "attach",
        args = { "${relativeFile}" }
    }

}
return M
