-- vim.o.colorcolumn=120 -- Highlight rows
-- vim.o.t_vb = ""
-- vim.o.list = true -- Show hidden characters
-- vim.o.number = true -- noi
-- vim.o.relativenumber = true -- Too slow in terminal
vim.o.ai = true -- Auto indent
vim.o.autoread = true -- read file when changed externally
vim.o.backspace = "indent,eol,start" -- backspace through everything in insert mode 
vim.o.backup = false
vim.o.cmdheight = 2 -- Show more command details
vim.o.cursorcolumn = false -- Show cursor
vim.o.cursorline = false -- Show cursor
vim.o.encoding = "utf-8" -- Set utf8 as standard encoding and en_US as the standard language
vim.o.errorbells = false -- No annoying sound on errors
vim.o.ffs = "unix,dos,mac" -- Use Unix as the standard file type
vim.o.fillchars = "stl:∙,stlnc: ,"
vim.o.guifont = "InputMono Light:h12"
vim.o.hid = true -- A buffer becomes hidden when it is abandoned
vim.o.history = 1000
vim.o.hlsearch = true -- Highlight search results
vim.o.ignorecase = true -- Ignore case when searching
vim.o.inccommand = "split"
vim.o.incsearch = true -- Makes search act like search in modern browsers
vim.o.laststatus = 2 -- Always show status bar
vim.o.lazyredraw = true -- Don't redraw while executing macros
vim.o.linespace = 2
vim.o.listchars = "trail:·,eol:˥,tab:» "
vim.o.magic = true -- For regular expressions turn magic on
vim.o.mat = 2 -- How many tenths of a second to blink when matching brackets
vim.o.ruler = true -- show current position
vim.o.scrolloff = 5 -- Show extra line
vim.o.secure = true
vim.o.shiftwidth = 4
vim.o.shortmess = "c"
vim.o.showcmd = true -- Show selected commands
vim.o.showmatch = true -- Show matching brackets when text indicator is over them
vim.o.si = true -- Smart indent
vim.o.sidescrolloff = 5 -- Show extra column
vim.o.signcolumn = "auto:3" -- Set width of gutter
vim.o.smartcase = true -- When searching try to be smart about cases 
vim.o.smarttab = true -- Be smart when using tabs ;)
vim.o.statusline = "%<%f%=%l:%c/%L%m"
vim.o.swapfile = false
vim.o.tabstop = 4
vim.o.termguicolors = true
vim.o.tm = 500
vim.o.visualbell = false
vim.o.wildignore = "*.o,*~,*.pyc,*.class,*.rkt~"
vim.o.wildmenu = true -- tab completion for system stuff
vim.o.wrap = true -- Wrap lines
vim.o.writebackup = false

vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1
vim.opt.colorcolumn = "120"

vim.g.python3_host_prog = "/usr/bin/python3"

vim.cmd([[
" fast escape
if ! has('gui_running')
    set ttimeoutlen=10
    augroup FastEscape
        autocmd!
        au InsertEnter * set timeoutlen=0
        au InsertLeave * set timeoutlen=1000
    augroup END
endif

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif

" Filetype specifics
autocmd Filetype ruby   setlocal sw=2 sts=2
autocmd FileType racket setlocal sw=2 sts=2
autocmd Filetype python setlocal expandtab tabstop=4 shiftwidth=4
autocmd FileType gitcommit setlocal tw=68 spell
autocmd FileType markdown setlocal tw=72 spell
]])
