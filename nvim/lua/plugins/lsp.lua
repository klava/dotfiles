-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(_, bufnr)
    vim.lsp.inlay_hint.enable(true, { bufnr = bufnr })

    vim.keymap.set("n", "K", vim.lsp.buf.hover, { noremap = true, silent = true, buffer = bufnr })
    vim.keymap.set("n", "[g", vim.diagnostic.goto_prev,
        { noremap = true, silent = true, buffer = bufnr })
    vim.keymap.set("n", "]g", vim.diagnostic.goto_next,
        { noremap = true, silent = true, buffer = bufnr })
    vim.keymap.set("n", "gD", vim.lsp.buf.declaration,
        { noremap = true, silent = true, buffer = bufnr, desc = "[g]o to [D]eclaration" })
    vim.keymap.set("n", "gd", function() require("telescope.builtin").lsp_definitions() end,
        { noremap = true, silent = true, buffer = bufnr, desc = "[g]o to [d]efinitions" })
    vim.keymap.set("n", "gi", function() require("telescope.builtin").lsp_implementations() end, {
        noremap = true,
        silent = true,
        buffer = bufnr,
        desc = "[g]o to [i]mplementations",
    })
    vim.keymap.set("n", "gr", function() require("telescope.builtin").lsp_references() end,
        { noremap = true, silent = true, buffer = bufnr, desc = "[g]o to [r]eferences" })

    vim.keymap.set("n", "<leader>ch", vim.lsp.buf.signature_help, {
        noremap = true,
        silent = true,
        buffer = bufnr,
        desc = "[c]ode [h]elp signature (LSP)",
    })
    vim.keymap.set("n", "<leader>cr", vim.lsp.buf.rename,
        { noremap = true, silent = true, buffer = bufnr, desc = "[c]ode [r]ename (LSP)" })
    vim.keymap.set("n", "<leader>cf", vim.lsp.buf.format,
        { noremap = true, silent = true, buffer = bufnr, desc = "[c]ode [f]ormat (LSP)" })
    vim.keymap.set("n", "<leader>ca", vim.lsp.buf.code_action,
        { noremap = true, silent = true, buffer = bufnr, desc = "[c]ode [a]ction (LSP)" })
    vim.keymap.set("v", "<leader>ca", vim.lsp.buf.code_action,
        { noremap = true, silent = true, buffer = bufnr, desc = "[c]ode [a]ction (LSP)" })

    -- bind('n', '<space>e', '<cmd>lua vim.diagnostic.open_float()<CR>', opts)
    -- bind('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
    -- bind('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
    -- bind('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
    -- bind('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
    -- bind('n', '<space>q', '<cmd>lua vim.diagnostic.setloclist()<CR>', opts)
    -- bind('n', '<space>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)
end

local pwd = vim.fn.getcwd()
local uid = vim.trim(vim.fn.system({ "id", "-u" }))
local gid = vim.trim(vim.fn.system({ "id", "-g" }))

return {
    "neovim/nvim-lspconfig",
    dependencies = { "saghen/blink.cmp", "folke/neodev.nvim", "rcarriga/nvim-dap-ui" },

    -- example using `opts` for defining servers
    opts = {
        servers = {
            clangd = {},
            cmake = {},
            cssls = {},
            cssmodules_ls = {},
            dockerls = {},
            golangci_lint_ls = {},
            html = {},
            jsonls = {},
            jsonnet_ls = {},
            lua_ls = {},
            ocamllsp = {},
            pylsp = {
                cmd = { "uv", "run", "pylsp" },
            },
            racket_langserver = {},
            ruff = {},
            rust_analyzer = {},
            terraformls = {},
            ts_ls = {},
            vimls = {},
            zls = {},

            bashls = { filetypes = { "sh", "bash", "zsh" } },

            yamlls = {
                settings = {
                    yaml = {
                        validate = false,
                        schemaStore = {
                            url = "https://www.schemastore.org/api/json/catalog.json",
                            enable = false,
                        },
                    },
                },
                cmd = {
                    "docker",
                    "run",
                    "--rm",
                    "-i",
                    "--user",
                    uid .. ":" .. gid,
                    "-v",
                    pwd .. ":" .. pwd,
                    "quay.io/redhat-developer/yaml-language-server:latest",
                    "--stdio",
                },
            },

            jdtls = { cmd = { "jdtls" }, root_dir = function() return vim.fn.getcwd() end },

            gopls = {
                settings = {
                    gopls = {
                        gofumpt = true,
                        usePlaceholders = false,
                        hints = {
                            assignVariableTypes = false,
                            compositeLiteralFields = false,
                            functionTypeParameters = false,
                            parameterNames = false,
                            rangeVariableTypes = true,
                        },
                    },
                },
            },
        },
    },

    config = function(_, opts)
        require("neodev").setup({
            library = {
                plugins = { "nvim-dap-ui", "neotest" },
                types = true,
            }
        })

        local lspconfig = require("lspconfig")

        for server, config in pairs(opts.servers) do
            config.capabilities = require("blink.cmp").get_lsp_capabilities(
                lspconfig.util.default_config.capabilities
            )
            config.flags = { debounce_text_changes = 50 }
            config.on_attach = on_attach
            lspconfig[server].setup(config)
        end
    end,
}
