return {
    {"arturgoms/moonbow.nvim", lazy = true},
    {"challenger-deep-theme/vim", lazy = true},
    {"haishanh/night-owl.vim", lazy = true},
    {"micke/vim-hybrid", lazy = true},
    {
        "rebelot/kanagawa.nvim",
        priority = 1000,
        config = function() require("kanagawa").load("dragon") end,
    },
    {"shatur/neovim-ayu", lazy = true},
    {"tiagovla/tokyodark.nvim", lazy = true},
}
