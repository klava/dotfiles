return {
    {
        "nvim-neotest/neotest",
        ft = { "python", "go", "zig" },
        dependencies = {
            "nvim-neotest/nvim-nio",
            "nvim-lua/plenary.nvim",
            "nvim-treesitter/nvim-treesitter",
            "nvim-neotest/neotest-python",
            "nvim-neotest/neotest-go",
            {
                "lawrence-laz/neotest-zig",
                version = "1.3.*",
            },
        },
        config = function()
            require("neotest").setup({
                adapters = {
                    require("neotest-go")({
                        experimental = { test_table = true },
                        args = { "-count=1", "-timeout=30s" },
                    }),
                    require("neotest-python")({
                        dap = { justMyCode = true },
                    }),
                    require("neotest-zig")({
                        dap = { adapter = "lldb", }
                    }),
                },
                log_level = 1,
                icons = { failed = "!", passed = "∙", running = "?", skipped = "s" },
            })

            vim.keymap.set("n", "<leader>ts", require("neotest").summary.toggle,
                { desc = "[t]est [s]ummary" })
            vim.keymap.set("n", "<leader>tc", require("neotest").run.stop,
                { desc = "[t]est [c]ancel/stop" })
            vim.keymap.set("n", "<leader>to",
                function() require("neotest").output.open({ enter = true }) end,
                { desc = "[t]est [o]utput" })
            vim.keymap.set("n", "<leader>tp", require("neotest").output_panel.toggle,
                { desc = "[t]est output [p]anel toggle" })
            vim.keymap.set("n", "<leader>tt", require("neotest").run.run,
                { desc = "[t]est current [t]est" })
            vim.keymap.set("n", "<leader>tr", require("neotest").run.run_last,
                { desc = "[t]est [r]erun last test" })
            vim.keymap.set("n", "<leader>tdr",
                function() require("neotest").run.run_last({ strategy = "dap" }) end,
                { desc = "[t]est [d]ebug last [r]un" })
            vim.keymap.set("n", "<leader>tdt",
                function() require("neotest").run.run({ strategy = "dap" }) end,
                { desc = "[t]est [d]ebug current [t]est" })
            vim.keymap.set("n", "<leader>tf",
                function() require("neotest").run.run(vim.fn.expand("%")) end,
                { desc = "[t]est current [f]ile" })
            vim.keymap.set("n", "<leader>tdf", function()
                require("neotest").run.run({ vim.fn.expand("%"), strategy = "dap" })
            end, { desc = "[t]est [d]ebug current [f]ile" })
        end,
    },
}
