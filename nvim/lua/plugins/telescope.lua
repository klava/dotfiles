local max_size = 100000
local preview_maker = function(filepath, bufnr, opts)
  opts = opts or {}

  filepath = vim.fn.expand(filepath)
  vim.loop.fs_stat(filepath, function(_, stat)
    if not stat then return end
    if stat.size > max_size then
      local cmd = {"head", "-c", max_size, filepath}
      require('telescope.previewers.utils').job_maker(cmd, bufnr, opts)
    else
      require('telescope.previewers').buffer_previewer_maker(filepath, bufnr, opts)
    end
  end)
end

return {
    {
        "nvim-telescope/telescope.nvim",
        dependencies = {
            {"nvim-lua/plenary.nvim"},
            {
                'nvim-telescope/telescope-fzf-native.nvim',
                build = 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release',
            }
        },
        config = function()
            require("telescope").setup {
                defaults = {
                    mappings = {
                        i = {["<c-q>"] = require("telescope.actions").send_selected_to_loclist},
                    },
                    buffer_previewer_maker = preview_maker,
                },
                pickers = {
                    find_files = {
                        theme = "ivy"
                    }
                }
            }
            require("telescope").load_extension("fzf")
        end,
    },
}
