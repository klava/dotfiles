return {
    {
        "rcarriga/nvim-dap-ui",
        ft = { "c", "cpp", "python", "go" },
        dependencies = { "mfussenegger/nvim-dap", "theHamsta/nvim-dap-virtual-text" },
        config = function()
            local dapconfig = require("dap-config")
            local dap = require("dap")
            local widgets = require("dap.ui.widgets")

            vim.keymap.set("n", "<leader>dc", dapconfig.continue, { desc = "[d]ebug [c]ontinue (DAP)" })
            vim.keymap.set("n", "<leader>dn", dap.step_over, { desc = "[d]ebug step [n]ext/over (DAP)" })
            vim.keymap.set("n", "<leader>di", dap.step_into, { desc = "[d]ebug step [i]nto (DAP)" })
            vim.keymap.set("n", "<leader>do", dap.step_out, { desc = "[d]step [o]ut (DAP)" })
            vim.keymap.set("n", "<leader>db", dap.toggle_breakpoint,
                { desc = "toggle [d]ebug [b]reakpoint (DAP)" })
            vim.keymap.set("n", "<leader>dp",
                function()
                    dap.set_breakpoint(vim.fn.input("Break condition:"))
                end, { desc = "set [d]ebug breakpoint [c]ondition (DAP)" })
            vim.keymap.set("n", "<leader>dl", function()
                dap.set_breakpoint(nil, nil, vim.fn.input("Log point message: "))
            end, { desc = "set [d]ebug [l]og point (DAP)" })
            vim.keymap.set("n", "<leader>dd", function() dapconfig.rerun() end,
                { desc = "[d]ebug [d]at again (DAP)" })
            vim.keymap.set("n", "<leader>dr", dap.repl.open, { desc = "[d]ebug open [r]epl (DAP)" })
            vim.keymap.set({ "n", "v" }, "<leader>dh", require("dap.ui.widgets").hover,
                { desc = "[d]ebug [h]over (DAP)" })
            vim.keymap.set({ "n", "v" }, "<leader>dp", require("dap.ui.widgets").preview,
                { desc = "[d]ebug [p]review (DAP)" })
            vim.keymap.set("n", "<leader>df", function()
                widgets.centered_float(widgets.frames)
            end, { desc = "[d]ebug show [f]rames (DAP)" })
            vim.keymap.set("n", "<leader>ds", function()
                widgets.centered_float(widgets.scopes)
            end, { desc = "[d]ebug show [s]copes (DAP)" })
            vim.keymap.set("n", "<leader>dq", dap.terminate, { desc = "[d]ebug [q]uit (DAP)" })
        end,
    },
}
