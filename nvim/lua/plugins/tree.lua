return {
    {
        "nvim-tree/nvim-tree.lua",
        dependencies = {"nvim-tree/nvim-web-devicons"},
        config = function()
            require("nvim-tree").setup({
                filters = {custom = {"^.git$"}},
                on_attach = function(bufnr)
                    local api = require "nvim-tree.api"

                    local function keymap_opts(desc)
                        return {
                            desc = "nvim-tree: " .. desc,
                            buffer = bufnr,
                            noremap = true,
                            silent = true,
                            nowait = true,
                        }
                    end

                    -- local function edit_or_open()
                    --     local node = api.tree.get_node_under_cursor()
                    --
                    --     if node.nodes ~= nil then
                    --         -- expand or collapse folder
                    --         api.node.open.edit()
                    --     else
                    --         -- open file
                    --         api.node.open.edit()
                    --         -- Close the tree if file was opened
                    --         -- api.tree.close()
                    --     end
                    -- end

                    local function close_or_step_up()
                        local node = api.tree.get_node_under_cursor()

                        if node.nodes ~= nil then
                            -- expand or collapse folder
                            api.node.navigate.parent_close()
                        else
                            api.node.navigate.parent()
                            -- open file
                            -- api.node.open.edit()
                            -- Close the tree if file was opened
                            -- api.tree.close()
                        end
                    end

                    -- default mappings
                    api.config.mappings.default_on_attach(bufnr)

                    -- custom mappings
                    -- vim.keymap.set("n", "<C-t>", api.tree.change_root_to_parent, opts("Up"))
                    vim.keymap.set("n", "<left>", close_or_step_up, keymap_opts("Close"))
                    vim.keymap.set("n", "<right>", api.node.open.edit, keymap_opts("Edit or open"))
                    vim.keymap.set("n", "?", api.tree.toggle_help, keymap_opts("Help"))

                end,
            })
        end,

    },
}
