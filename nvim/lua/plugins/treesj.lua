return {
    "Wansmer/treesj",
    keys = {
        {
            "<leader>cj",
            function()
                -- print("---------- join start --------")
                require("treesj").join()
                -- print("---------- join end --------")
            end,
            silent = true,
            desc = "[c]ode [j]oin arguments",
        },
        {
            "<leader>cs",
            function()
                -- print("---------- split start --------")
                require("treesj").split()
                -- print("---------- split end --------")
            end,
            silent = true,
            desc = "[c]ode [s]plit arguments",
        },
    },
    dependencies = { "nvim-treesitter/nvim-treesitter" }, --[[ if you install parsers with `nvim-treesitter` ]]
    config = function()
        local utils = require("treesj.langs.utils")
        require("treesj").setup({
            use_default_keymaps = false,
            max_join_length = 1000,
            langs = {
                zig = {
                    initializer_list = utils.set_preset_for_list({
                        both = {
                            recursive_ignore = { "assignment_expression" },
                        }
                    }),
                    call_expression = {
                        both = {
                            separator = ",",
                            shrink_node = { from = '(', to = ')' },
                        },
                        split = {
                            last_separator = true,
                        },
                        join = {
                            space_in_brackets = false,
                        },
                    },
                },
            },
        })
    end,
}
