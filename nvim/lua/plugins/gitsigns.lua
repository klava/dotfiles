return {
    "lewis6991/gitsigns.nvim",
    config = function()
        require("gitsigns").setup({
            on_attach = function(bufnr)
                local gitsigns = require("gitsigns")

                local function map(mode, l, r, opts)
                    opts = opts or {}
                    opts.buffer = bufnr
                    vim.keymap.set(mode, l, r, opts)
                end

                -- Navigation
                map("n", "]h", function()
                    if vim.wo.diff then
                        vim.cmd.normal({"]c", bang = true})
                    else
                        gitsigns.nav_hunk("next")
                    end
                end)

                map("n", "[h", function()
                    if vim.wo.diff then
                        vim.cmd.normal({"[c", bang = true})
                    else
                        gitsigns.nav_hunk("prev")
                    end
                end)

                -- Actions
                map("n", "<leader>hs", gitsigns.stage_hunk, {desc = "[h]unk [s]tage"})
                map("n", "<leader>hr", gitsigns.reset_hunk, {desc = "[h]unk [r]eset"})
                map("v", "<leader>hs",
                    function()
                    gitsigns.stage_hunk {vim.fn.line("."), vim.fn.line("v")}
                end, {desc = "[h]unk [s]tage"})
                map("v", "<leader>hr",
                    function()
                    gitsigns.reset_hunk {vim.fn.line("."), vim.fn.line("v")}
                end, {desc = "[h]unk [r]eset"})
                map("n", "<leader>hS", gitsigns.stage_buffer, {desc = "[h]unk [S]tage buffer"})
                map("n", "<leader>hu", gitsigns.undo_stage_hunk, {desc = "[h]unk [u]ndo"})
                map("n", "<leader>hR", gitsigns.reset_buffer, {desc = "[h]unk [R]eset buffer"})
                map("n", "<leader>hp", gitsigns.preview_hunk, {desc = "[h]unk [p]review"})
                map("n", "<leader>hb", function() gitsigns.blame_line {full = true} end,
                    {desc = "[h]unk [b]lame"})
                map("n", "<leader>hl", gitsigns.toggle_current_line_blame, {desc = "[h]unk [b]lame"})
                map("n", "<leader>hd", gitsigns.toggle_deleted, {desc = "[h]unk [d]deleted toggle"})

                -- Text object
                map({"o", "x"}, "ih", ":<C-U>Gitsigns select_hunk<CR>")
            end,
            current_line_blame = true,
        })
    end,
}
