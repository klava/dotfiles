return {
    "fatih/vim-nginx",              --
    "folke/neodev.nvim",            -- configure lua_ls for neovim development
    "junegunn/vim-easy-align",      --
    "neovim/nvim-lspconfig",        --
    "sbdchd/neoformat",             --
    "terryma/vim-multiple-cursors", --
    "tomtom/tcomment_vim",          -- Comment/uncomment things
    "tpope/vim-eunuch",             -- UNIX shell commands, :Move, :Rename etc.
    "tpope/vim-fugitive",           -- Git niceties
    "tpope/vim-repeat",             --
    "tpope/vim-sleuth",             -- Guess indentation
    "tpope/vim-surround",           --
    "vim-scripts/matchit.zip",      -- do/end match
    { "ekalinin/Dockerfile.vim", ft = { "Dockerfile" } },
    { "eraserhd/parinfer-rust",  build = "cargo build --release" },
    { "google/vim-jsonnet",      ft = { "jsonnet" } },
    { "lervag/vimtex",           ft = { "plaintex" } }, -- LaTeX stuff
    { "let-def/ocp-indent-vim",  ft = { "ocaml" } }, -- Ocaml indentation
    { "sindrets/diffview.nvim",  dependencies = "nvim-lua/plenary.nvim" },
    { "udalov/kotlin-vim",       ft = { "kotlin" } },
    { "wlangstroth/vim-racket",  ft = { "racket" } }, -- Racket programming language
    {
        "echasnovski/mini.bracketed",            -- replaces tpope/vim-unimpaired
        config = function() require("mini.bracketed").setup({ file = { suffix = "" } }) end,
    },
}
