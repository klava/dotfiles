local set = vim.keymap.set

-- vim normal
set("n", "<C-Left>", "<C-w>h", { desc = "left window" })
set("n", "<C-Down>", "<C-w>j", { desc = "down window" })
set("n", "<C-Up>", "<C-w>k", { desc = "up window" })
set("n", "<C-Right>", "<C-w>l", { desc = "right window" })
set("n", "<leader><leader>", "<c-^>", { desc = "go to previous file" })

-- vim terminal
set("t", "<C-Left>", "<C-\\><C-N><C-w>h", { desc = "left window" })
set("t", "<C-Down>", "<C-\\><C-N><C-w>j", { desc = "down window" })
set("t", "<C-Up>", "<C-\\><C-N><C-w>k", { desc = "up window" })
set("t", "<C-Right>", "<C-\\><C-N><C-w>l", { desc = "right window" })
set("t", "<Esc>", "<C-\\><C-n>", { desc = "term to normal mode" })

-- EasyAlign
set("x", "ga", "<Plug>(EasyAlign)", { desc = "[g]lobal [a]lign/select characters" })
set("n", "ga", "<Plug>(EasyAlign)", { desc = "[g]lobal [a]lign/select characters" })

-- Code (Neoformat, etc)
set("n", "<leader>cf", ":Neoformat<CR>",
    { noremap = true, silent = true, desc = "[c]ode [f]ormat (Neoformat)" })

-- git
set("n", "<leader>gh", ":DiffviewFileHistory %<CR>",
    { desc = "[g]it file[h]istory (Diffview)" })
set("n", "<leader>gd", ":DiffviewOpen<CR>", { desc = "[g]it [d]iff (Diffview)" })
set("n", "<leader>gs", ":Git<CR>", { desc = "[g]it [s]tatus (Fugitive)" })
set("n", "<leader>gq", ":DiffviewClose<CR>", { desc = "[g]it [q]uit (Diffview)" })

-- nvim-tree
set("n", "<c-h>", ":NvimTreeToggle<CR>", { desc = "File browser (nvim-tree)" })

-- telescope
local tb = require("telescope.builtin")
function vim.getVisualSelection()
    vim.cmd("noau normal! \"vy\"")
    local text = vim.fn.getreg("v")
    vim.fn.setreg("v", {})

    text = string.gsub(text, "\n", "")
    if #text > 0 then
        return text
    else
        return ""
    end
end

set("n", "<leader>sf", tb.find_files, { desc = "[s]earch [f]iles (telescope)" })
set("n", "<leader>sF", function() tb.find_files({ hidden = true }) end,
    { desc = "[s]earch with hidden [F]iles (telescope)" })
set("n", "<leader>sb", tb.buffers, { desc = "[s]earch [b]uffers (telescope)" })
set({ "n", "v" }, "<leader>ss", tb.resume,
    { desc = "[s]erach [s]ame again (telescope)" })
set({ "n", "v" }, "<leader>se", tb.git_status,
    { desc = "[s]erach [e]dited (telescope)" })
set("n", "<leader>sg", tb.current_buffer_fuzzy_find,
    { desc = "[s]erach [g]rep current buffer (telescope)" })
set("v", "<leader>sg", function()
    local text = vim.getVisualSelection()
    tb.current_buffer_fuzzy_find({ default_text = text })
end, { desc = "[s]erach [g]rep current buffer with selection (telescope)" })
set("n", "<leader>sG", tb.live_grep,
    { desc = "[s]earch [G]rep global (telescope)" })
set("v", "<leader>sG", function()
    local text = vim.getVisualSelection()
    tb.live_grep({ default_text = text })
end, { desc = "[s]earch [G]rep global (telescope)" })
set("n", "<leader>sc", tb.commands, { desc = "[s]erach [c]ommands (telescope)" })
set("n", "<leader>sd", tb.diagnostics,
    { desc = "[s]erach [d]iagnostics (telescope)" })
set("n", "<leader>sh", tb.help_tags, { desc = "[s]erach [h]help (telescope)" })
set("n", "<leader>sk", tb.keymaps, { desc = "[s]erach [k]keymap (telescope)" })
set("n", "<leader>sl", tb.loclist, { desc = "[s]erach [l]oclist (telescope)" })
set("n", "<leader>sq", tb.quickfix, { desc = "[s]erach [q]uickfix (telescope)" })
set("n", "<leader>sz", tb.spell_suggest,
    { desc = "[s]erach [z]pellings (telescope)" })

