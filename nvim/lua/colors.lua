vim.cmd([[
syntax enable
]])

vim.cmd.highlight({ "StatusLine", "ctermbg=Black ctermfg=White guibg=Black guifg=White term=NONE gui=NONE" })
vim.cmd.highlight({ "StatusLineNC", "ctermbg=Black ctermfg=White guibg=Black guifg=White term=NONE gui=NONE" })

vim.cmd.highlight({ "HighlightedyankRegion", "cterm=reverse gui=reverse" })
vim.api.nvim_create_autocmd({ "TextYankPost" }, {
    desc = "Highlight yanked text",
    callback = function()
        vim.highlight.on_yank {
            higroup = (vim.fn["hlexists"]("HighlightedyankRegion") > 0 and "HighlightedyankRegion" or "IncSearch"),
            timeout = 100
        }
    end
})

vim.cmd([[
" Set color of gitgutter
highlight clear SignColumn

" OSTTRA colors
" highlight Normal guibg='#212222' guifg='#00afaf'
]])
