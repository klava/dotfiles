export LANG=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export LC_TIME=sv_SE.UTF-8
export MOZ_ENABLE_WAYLAND=1
export GOPATH=$HOME/.local/go
export CC=clang
export CXX=clang++

typeset -TUx PATH path
path=(
	"$HOME/.atuin/bin"
	"$HOME/.cargo/bin"
	"$HOME/.dotnet/tools"
	"$HOME/.local/bin"
	"$HOME/.local/go/bin"
	"$HOME/.local/zig"
	"$HOME/.local/lua-language-server/bin"
	"$HOME/.luarocks/bin"
	"$HOME/.npm-global/bin"
	"$HOME/dotfiles/bin"
	/lib/llvm-15/bin
	/opt/cmake/bin
	/opt/nodejs/bin
	/opt/nvim-linux64/bin
	/bin
	/sbin
	/snap/bin
	/usr/bin
	/usr/libexec
	/usr/local/bin
	/usr/local/cuda/bin
	/usr/local/go/bin
	/usr/local/node/bin
	/usr/local/sbin
	/usr/sbin
)

typeset -TUx LD_LIBRARY_PATH ld_library_path
ld_library_path+=(
	"$HOME/.local/lib/x86_64-linux-gnu"
	/usr/lib
	/usr/local/lib
)

typeset -TUx PKG_CONFIG_PATH pkg_config_path
pkg_config_path+=(
	"$HOME/.local/lib/x86_64-linux-gnu/pkgconfig"
	"$HOME/.local/lib64/pkgconfig"
	"$HOME/.local/share/pkgconfig"
)

typeset -TUx XDG_DATA_DIRS xdg_data_dirs
xdg_data_dirs+=(
	/usr/local/share
	/usr/share
	/var/lib/snapd/desktop
)
