#!/usr/bin/env bash
set -e

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
YES=$1

function prompt {
	if [ "$YES" = "-y" ]; then
		return 0
	fi

	read -p "$1 (Y/n)" -n 1 -r
	echo
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		return 0
	fi
	return 1
}

function install {
	if [ -f "$2" ]; then
		if prompt "'$2' already exist. Do you wish to overwrite?"; then
			ln -fs "$DIR"/"$1" "$2"
		else
			echo "Skipping '$2'."
		fi
	else
		mkdir -p "$(dirname "$2")"
		ln -s "$DIR"/"$1" "$2" || (
			echo "$2 may be a broken symlink - forcing..."
			ln -fs "$DIR"/"$1" "$2"
		)
		echo "Created $2"
	fi
}

function install_dir {
	if [ -d "$2" ]; then
		if prompt "'$2' already exist. Do you wish to overwrite?"; then
			rm -rf "$2"
			ln -fs "$DIR"/"$1"/ "$2"
		else
			echo "Skipping '$2'."
		fi
	else
		mkdir -p "$(dirname "$2")"
		ln -fs "$DIR"/"$1"/ "$2"
		echo "Created $2"
	fi
}

function clone {
	if [ -d "$2" ]; then
		if prompt "'$2' already exist. Do you wish to overwrite?"; then
			git clone "$1" "$2"
		else
			echo "Skipping cloning of '$1' to '$2'."
		fi
	else
		git clone "$1" "$2"
		echo "Created $2"
	fi
}

install Xdefaults ~/.Xdefaults
install foot.ini ~/.config/foot/foot.ini
install gitconfig ~/.gitconfig
install ideavimrc ~/.ideavimrc
install ipython_config.py ~/.ipython/profile_default/ipython_config.py
install profile ~/.profile
install ssh.conf ~/.ssh/config
install sway.conf ~/.config/sway/config
install tmux.conf ~/.tmux.conf
install waybar.css ~/.config/waybar/style.css
install waybar.json ~/.config/waybar/config
install zshenv ~/.zshenv
install zprofile ~/.bash_profile
install zprofile ~/.zprofile
install zshrc ~/.zshrc
install_dir fonts ~/.fonts
install_dir oh-my-zsh ~/.oh-my-zsh-custom
install_dir nvim ~/.config/nvim
install_dir vsnip ~/.vsnip

clone https://github.com/ohmyzsh/ohmyzsh.git ~/.oh-my-zsh
clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh-custom/plugins/zsh-syntax-highlighting
clone https://github.com/zsh-users/zsh-autosuggestions.git ~/.oh-my-zsh-custom/plugins/zsh-autosuggestions
clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

echo "--------------------------------------------------------------------------------"
echo "                               INSTALLATION COMPLETE                            "
echo "--------------------------------------------------------------------------------"
echo ""
echo "Consider additional installs:"
echo ""
echo sudo apt install zsh jq ripgrep shellcheck shfmt fzf cmake
echo sudo chsh -s /usr/bin/zsh $USER
echo install-node
echo install-neovim
echo install-go
echo install-kubectl
echo curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
echo curl --proto '=https' --tlsv1.2 -LsSf https://setup.atuin.sh | sh
echo npm i -g bash-language-server
echo ""
echo "You should probably restart your shell. Good luck."
