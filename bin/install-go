#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail # may get annoying
if [[ "${TRACE-0}" == "1" ]]; then
    set -o xtrace
fi

main() {
    WORKDIR=/tmp/install-go
    rm -rf $WORKDIR
    mkdir $WORKDIR
    cd $WORKDIR
    FILENAME=$(curl -s https://go.dev/dl/ | rg "downloadBox.+linux" | sed -E 's/.+href="\/dl\/([^"]+)".+/\1/')
    URL="https://go.dev/dl/$FILENAME"
    wget --show-progress "$URL"
    sudo rm -rf /usr/local/go && sudo tar -C /usr/local -xzf "$FILENAME"

    echo "Go has been installed. Consider:"
    echo "go install golang.org/x/tools/gopls@latest"
    echo "go install github.com/go-delve/delve/cmd/dlv@latest"
    echo curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b "$(go env GOPATH)/bin" v1.62.2
    echo "go install github.com/nametake/golangci-lint-langserver@latest"
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    cd "$(dirname "$0")"
    main "$@"
fi
